import 'package:corte1/src/pages/styles/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:corte1/src/pages/styles/colors.dart';
import 'package:corte1/src/routes/routes.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MainApp());
  });
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Corte 1',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: colorLightCircle,
        accentColor: colorDarkCircle,
        cursorColor: colorLightCircle,
        inputDecorationTheme: InputDecorationTheme(
          labelStyle: labelTextStyle,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: colorLightCircle, width: 2.0),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: colorLightCircle, width: 1.0),
          ),
        ),
      ),
      initialRoute: 'login',
      routes: getApplicationRoutes(),
    );
  }
}
