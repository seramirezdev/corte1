import 'dart:convert';

class UsuarioEntity {
    int id;
    String correo;
    String password;
    String activo;

    

    UsuarioEntity({
        this.id,
        this.correo,
        this.password,
        this.activo,
    });

    factory UsuarioEntity.fromJson(String str) => UsuarioEntity.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory UsuarioEntity.fromMap(Map<String, dynamic> json) => new UsuarioEntity(
        id: json["id"],
        correo: json["correo"],
        password: json["password"],
        activo: json["activo"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "correo": correo,
        "password": password,
        "activo": activo,
    };
}
