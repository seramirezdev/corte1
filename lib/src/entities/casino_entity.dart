import 'dart:convert';

class CasinoEntity {
    int id;
    String name;
    String capacity;
    String phoneNumber;
    String address;
    String city;

    CasinoEntity({
        this.id,
        this.name,
        this.capacity,
        this.phoneNumber,
        this.address,
        this.city,
    });

    factory CasinoEntity.fromJson(String str) => CasinoEntity.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory CasinoEntity.fromMap(Map<String, dynamic> json) => new CasinoEntity(
        id: json["id"],
        name: json["name"],
        capacity: json["capacity"],
        phoneNumber: json["phoneNumber"],
        address: json["address"],
        city: json["city"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "capacity": capacity,
        "phoneNumber": phoneNumber,
        "address": address,
        "city": city,
    };
}
