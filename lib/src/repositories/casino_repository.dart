import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:corte1/src/entities/casino_entity.dart';

const String URL_SERVICE = "http://35.224.185.226/casino/";

class CasinoRepository {
  Future<List<CasinoEntity>> fetchCasinos() async {
    var casinos = <CasinoEntity>[];

    final String url = URL_SERVICE + "all";

    var client = http.Client();

    try {
      var response = await client.get(url);

      casinos = _decodeBodyListCasinos(response.body);
    } catch (e) {
      print("Error en la petición");
      print(e);
      return Future.error("Error con la peticón al servidor");
    } finally {
      client.close();
    }

    return casinos;
  }

  Future saveCasino(CasinoEntity casino) async {
    var client = http.Client();

    final String url = URL_SERVICE + "insert";

    try {
      await client.post(url, body: casino.toJson(), headers: {
        'Content-Type': 'application/json'
      });
    } catch (e) {
      print('Error en la petición');
      print(e);
    } finally {
      client.close();
    }
  }

  Future deleteCasino(int id) async {
    var client = http.Client();

    final String url = URL_SERVICE + "delete?id=$id";

    try {
      await client.delete(url);
    } catch (e) {
      print('Error en la petición');
      print(e);
    } finally {
      client.close();
    }
  }

  Future updateCasino(CasinoEntity casino) async {
    var client = http.Client();

    final String url = URL_SERVICE + "update";

    try {
      await client.put(url, body: casino.toJson());
    } catch (e) {
      print('Error en la petición');
      print(e);
    } finally {
      client.close();
    }
  }

  List<CasinoEntity> _decodeBodyListCasinos(String responseBody) {
    final List parsed = json.decode(responseBody);

    return parsed
        .map((json) => CasinoEntity.fromMap(json))
        .toList();
  }
}
