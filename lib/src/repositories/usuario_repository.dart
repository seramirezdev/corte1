import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:corte1/src/entities/usuario_entity.dart';

const String URL_SERVICE = "http://35.224.185.226/usuario/";

class UsuarioRepository {
  Future<List<UsuarioEntity>> fetchUsuarios() async {
    var usuarios = <UsuarioEntity>[];

    final String url = URL_SERVICE + "all";

    var client = http.Client();

    try {
      var response = await client.get(url);

      usuarios = _decodeBodyListUsuarios(response.body);
    } catch (e) {
      print("Error en la petición");
      print(e);
      return Future.error("Error con la peticón al servidor");
    } finally {
      client.close();
    }

    return usuarios;
  }

  Future saveUsuario(UsuarioEntity usuario) async {
    var client = http.Client();

    final String url = URL_SERVICE + "insert";

    try {
      await client.post(url, body: usuario.toJson(), headers: {
        'Content-Type': 'application/json'
      });
    } catch (e) {
      print('Error en la petición');
      print(e);
    } finally {
      client.close();
    }
  }

  Future deleteUsuario(int id) async {
    var client = http.Client();

    final String url = URL_SERVICE + "delete?id=$id";

    try {
      await client.delete(url);
    } catch (e) {
      print('Error en la petición');
      print(e);
    } finally {
      client.close();
    }
  }

  Future updateUsuario(UsuarioEntity usuario) async {
    var client = http.Client();

    final String url = URL_SERVICE + "update";

    try {
      await client.put(url, body: usuario.toJson());
    } catch (e) {
      print('Error en la petición');
      print(e);
    } finally {
      client.close();
    }
  }

  List<UsuarioEntity> _decodeBodyListUsuarios(String responseBody) {
    final List<dynamic> parsed = json.decode(responseBody);

    return parsed
        .map((json) => UsuarioEntity.fromMap(json))
        .toList();
  }


  Future <bool> loginUsuario(UsuarioEntity usuario) async {
    var client = http.Client();

    final String url = URL_SERVICE + "login";

    try {
      var response = await client.post(url, body: usuario.toJson(), headers: {
        'Content-Type': 'application/json'
      });
      print(response.body.isNotEmpty);
      return response.body.isNotEmpty;
    } catch (e) {
      print('Error en la petición');
      print(e);
      return false;
    } finally {
      client.close();
    }
  }
}
