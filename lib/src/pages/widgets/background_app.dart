import 'package:corte1/src/pages/styles/colors.dart';
import 'package:flutter/material.dart';

class BackgroundApp extends StatelessWidget {
  double widthCircleMini;
  double widthCircleNormal;
  double widthSpace;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {

        final width = constraints.maxWidth;
        widthCircleMini = width * 0.12;
        widthCircleNormal = width * 0.23;
        widthSpace = width * 0.25;

        return Stack(
          children: <Widget>[
            Container(
              color: colorBackground,
            ),
            Positioned(
              right: -widthCircleNormal / 2,
              top: 20.0,
              child: _createCircleRowRight(),
            ),
            Positioned(
              bottom: 20.0,
              left: -widthCircleNormal / 2,
              child: _createCircleRowLeft(),
            )
          ],
        );
      },
    );
  }

  Widget _createCircleRowLeft() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        _createCircle(false),
        SizedBox(width: widthSpace * 0.1),
        _createCircle(true),
      ],
    );
  }

  Widget _createCircleRowRight() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        _createCircle(true),
        SizedBox(width: widthSpace * 0.1),
        _createCircle(false),
      ],
    );
  }

  Widget _createCircle(isMini) {
    final size = isMini ? widthCircleMini : widthCircleNormal;

    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: colorDarkCircle,
        gradient: LinearGradient(
            colors: [colorLightCircle, colorDarkCircle],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter),
        boxShadow: [
          BoxShadow(
            blurRadius: 10,
            offset: Offset(0, 3),
            color: colorDarkCircle,
            spreadRadius: -2,
          ),
        ],
      ),
    );
  }
}
