import 'package:corte1/src/pages/styles/colors.dart';
import 'package:flutter/material.dart';

final textTitleStyle = TextStyle(
  fontSize: 32.0,
  color: colorTextTitle,
  fontWeight: FontWeight.bold
);

final textTitleAlertStyle = TextStyle(
  fontSize: 26.0,
  color: colorTextTitle,
  fontWeight: FontWeight.bold
);

final labelTextStyle = TextStyle(color: colorLightCircle.withAlpha(200));