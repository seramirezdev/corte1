import 'package:flutter/material.dart';

final Color colorDarkCircle = Color.fromRGBO(0, 81, 173, 1);
final Color colorLightCircle = Color.fromRGBO(35, 118, 186, 1);
final Color colorBackground = Colors.white;

final Color colorTextTitle = Colors.grey[900];