import 'package:corte1/src/entities/usuario_entity.dart';
import 'package:corte1/src/pages/styles/colors.dart';
import 'package:corte1/src/pages/styles/text_styles.dart';
import 'package:corte1/src/pages/widgets/background_app.dart';
import 'package:corte1/src/repositories/usuario_repository.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _checkRemenber = true;
  TextEditingController _loginController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  final keyform = GlobalKey<FormState>();
  String showError = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            BackgroundApp(),
            Positioned(
              top: 45,
              left: 30,
              child: Text('Inicio de sesión', style: textTitleStyle),
            ),
            _createForm(),
            Positioned(
              bottom: 45,
              right: 30,
              child: Text('Registrarse', style: textTitleStyle),
            )
          ],
        ),
      ),
    );
  }

  Widget _createForm() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Card(
            margin: EdgeInsets.symmetric(horizontal: 30.0),
            elevation: 8.0,
            child: Container(
              padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 40.0),
              child: Form(
                key: keyform,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      validator: (value){
                        if(value.isEmpty){
                          return "Debe ingresar un correo";
                        } 
                        return null;
                      },
                      controller: _loginController,
                      decoration: InputDecoration(
                        prefixIcon:
                            Icon(Icons.person_outline, color: colorLightCircle),
                        labelText: 'Correo electrónico',
                      ),
                    ),
                    TextFormField(
                      validator: (value){
                        if(value.isEmpty){
                          return "Debe ingresar una contraseña";
                        } 
                        return null;
                      },
                      obscureText: true,
                      controller: _passwordController,
                      decoration: InputDecoration(
                        prefixIcon:
                            Icon(Icons.lock_outline, color: colorLightCircle),
                        labelText: 'Contraseña',
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 15,),
          Text(showError, style: TextStyle(color: Colors.red),),
          Container(
            margin: EdgeInsets.only(left: 20.0, top: 15.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Checkbox(
                  value: _checkRemenber,
                  onChanged: (val) {
                    setState(() {
                      _checkRemenber = val;
                    });
                  },
                ),
                Text('Recuerdame'),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    login();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    padding:
                        EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
                    child: Text('LOGIN',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [colorLightCircle, colorDarkCircle],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10,
                          offset: Offset(0, 3),
                          color: colorDarkCircle,
                          spreadRadius: -2,
                        )
                      ],
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25.0),
                        bottomLeft: Radius.circular(25.0),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  void login() async{
    if(keyform.currentState.validate()){
      var correo = _loginController.text;
      var password = _passwordController.text;
      UsuarioEntity usuario = UsuarioEntity(correo: correo, password: password);
      var ingreso = await UsuarioRepository().loginUsuario(usuario);
      if(ingreso){
        Navigator.pushNamed(context, 'casino_list');
      } else {
        setState(() {
         showError = "Credenciales incorrectas"; 
        });
      }
    }
    
  }
}
