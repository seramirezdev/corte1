import 'package:corte1/src/entities/casino_entity.dart';
import 'package:corte1/src/pages/styles/colors.dart';
import 'package:corte1/src/pages/styles/text_styles.dart';
import 'package:corte1/src/repositories/casino_repository.dart';
import 'package:flutter/material.dart';

class AlertDialogAdd extends StatefulWidget {
  final bool isCreate;
  final CasinoEntity casinoEntity;
  final Function updateList;

  const AlertDialogAdd({Key key, this.isCreate, this.casinoEntity, this.updateList})
      : super(key: key);

  @override
  _AlertDialogAddState createState() => _AlertDialogAddState();
}

class _AlertDialogAddState extends State<AlertDialogAdd> {
  final _formKey = GlobalKey<FormState>();
  final repository = CasinoRepository();
  CasinoEntity _casino;

  @override
  void initState() {
    super.initState();
    _casino =
        widget.casinoEntity == null ? CasinoEntity() : widget.casinoEntity;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: _dialogContent(context),
    );
  }

  Widget _dialogContent(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Builder(
        builder: (context) => Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                widget.isCreate ? 'Agregar' : 'Actualizar' + ' Casino',
                style: textTitleAlertStyle,
                textAlign: TextAlign.center,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Nombre'),
                initialValue: _casino?.name,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Ingresa el nombre del casino';
                  }
                  return null;
                },
                onSaved: (val) => setState(() => _casino?.name = val),
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Dirección'),
                initialValue: _casino?.address,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Ingresa una dirección para el casino';
                  }
                  return null;
                },
                onSaved: (val) => setState(() => _casino?.address = val),
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Teléfono'),
                initialValue: _casino?.phoneNumber,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Ingresa un teléfono para el casino';
                  }
                  return null;
                },
                onSaved: (val) => setState(() => _casino?.phoneNumber = val),
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Capacidad'),
                initialValue: _casino?.capacity,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Ingresa una capacidad para el casino';
                  }
                  return null;
                },
                onSaved: (val) => setState(() => _casino?.capacity = val),
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Ciudad'),
                initialValue: _casino?.city,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Ingresa una ciudad para el casino';
                  }
                  return null;
                },
                onSaved: (val) => setState(() => _casino?.city = val),
              ),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: RaisedButton(
                  color: colorDarkCircle,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                  onPressed: () {
                    final form = _formKey.currentState;
                    if (form.validate()) {
                      form.save();
                      print("Aquí");
                      print(_casino);
                      if (widget.isCreate) {
                        repository.saveCasino(_casino);
                      } else {
                        repository.updateCasino(_casino);
                      }
                      
                      Navigator.pop(context);
                      widget.updateList();
                    }
                  },
                  child: Text('Guardar'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
