import 'package:corte1/src/entities/casino_entity.dart';
import 'package:corte1/src/pages/casino/widgets/alert_dialog_add.dart';
import 'package:corte1/src/repositories/casino_repository.dart';
import 'package:flutter/material.dart';

class CasinoItemWidget extends StatelessWidget {
  final CasinoEntity casino;
  final Function updateList;
  const CasinoItemWidget({Key key, this.casino, this.updateList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final repository = CasinoRepository();

    return ListTile(
      title: Text(casino.name),
      subtitle: Text.rich(
        TextSpan(
          children: [
            TextSpan(text: 'Capacidad: ${casino.capacity}\n'),
            TextSpan(text: 'Ciudad: ${casino.city}\n'),
            TextSpan(text: 'Dirección: ${casino.address}\n'),
          ],
        ),
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          IconButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (_) {
                  return AlertDialogAdd(isCreate: false, casinoEntity: casino,);
                },
              );
            },
            icon: Icon(Icons.update, color: Colors.red),
          ),
          IconButton(
            onPressed: () async{
              await repository.deleteCasino(casino.id);
              updateList();
            },
            icon: Icon(
              Icons.delete,
              color: Colors.orange,
            ),
          ),
        ],
      ),
    );
  }
}
