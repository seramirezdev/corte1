import 'package:flutter/material.dart';

import 'package:corte1/src/entities/casino_entity.dart';
import 'package:corte1/src/pages/casino/widgets/casino_item_widget.dart';

class CasinoListWidget extends StatelessWidget {
  final List<CasinoEntity> casinos;
  final Function updateList;

  const CasinoListWidget({Key key, this.casinos, this.updateList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: casinos.length,
      itemBuilder: (context, index) {
        return CasinoItemWidget(casino: casinos[index], updateList: updateList,);
      },
    );
  }
}
