import 'package:corte1/src/entities/casino_entity.dart';
import 'package:corte1/src/pages/casino/widgets/alert_dialog_add.dart';
import 'package:corte1/src/pages/casino/widgets/casino_list_widget.dart';
import 'package:corte1/src/pages/styles/colors.dart';
import 'package:corte1/src/repositories/casino_repository.dart';
import 'package:flutter/material.dart';

class CasinoListPage extends StatefulWidget {
  @override
  _CasinoListPageState createState() => _CasinoListPageState();
}

class _CasinoListPageState extends State<CasinoListPage> {
  final repository = CasinoRepository();
  Future<List<CasinoEntity>> casinos;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _updateList();
  }
  @override
  Widget build(BuildContext context) {
    

    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de casinos'),
        centerTitle: true,
        backgroundColor: colorLightCircle,
        actions: <Widget>[
          IconButton(
            onPressed: () {
              _showAlertDialogAddCasino(context);
            },
            icon: Icon(Icons.add),
          )
        ],
      ),
      body: FutureBuilder(
        future: casinos,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            print(snapshot.error);
            return Center(
              child: Text(
                'Problemas de conexión con el servidor',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.grey[400], fontSize: 24.0),
              ),
            );
          }

          return snapshot.hasData
              ? CasinoListWidget(casinos: snapshot.data, updateList: _updateList,)
              : Center(
                  child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Cargando información...',
                      style: TextStyle(color: Colors.grey[400], fontSize: 24.0),
                    ),
                    CircularProgressIndicator(),
                  ],
                ));
        },
      ),
    );
  }

  void _showAlertDialogAddCasino(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialogAdd(isCreate: true, updateList: _updateList,);
      },
    );
  }

  void _updateList() {
    
    setState((){
      casinos = repository.fetchCasinos();
    });
  }
}
