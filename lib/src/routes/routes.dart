import 'package:corte1/src/pages/casino/casino_list_page.dart';
import 'package:corte1/src/pages/login/login_page.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    'login': (context) => LoginPage(),
    'casino_list': (context) => CasinoListPage(),
  };
}
